﻿#include <stdio.h>
int main(){
    int arr[4][3];
    int noZeroNumbers = 0;
    printf("arr\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 3; j++) {
            arr[i][j] = i - j;
            printf("%2d ", arr[i][j]);
            if (arr[i][j]) noZeroNumbers++;
        }
        printf("\n");
    }
    printf("There are %d numbers that dont equal 0\n", noZeroNumbers);
    return 0;
}