﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define ARRAY_SIZE 10
void randomizeArray(int array[][ARRAY_SIZE]) {
    for (int i = 0; i < ARRAY_SIZE; i++) {
        for (int j = 0; j < ARRAY_SIZE; j++) {
            array[i][j] = rand() % 1000;
        }
    }
}
void printArray(int array[][ARRAY_SIZE]) {
    printf("array\n");
    for (int i = 0; i < ARRAY_SIZE; i++) {
        for (int j = 0; j < ARRAY_SIZE; j++) {
            printf("%3d ", array[i][j]);
        }
        printf("\n");
    }
}
int main(){
    srand(time(0));
    int array[ARRAY_SIZE][ARRAY_SIZE] = { 0 };
    randomizeArray(array);
    printArray(array);
    int maxi = 0, maxj = 0;
    for (int i = 0; i < ARRAY_SIZE; i++) 
        for (int j = 0; j < ARRAY_SIZE; j++)        
            if (i <= ARRAY_SIZE - 1 - j && array[maxi][maxj] < array[i][j]) {
                maxi = i;
                maxj = j;
            }
    printf("Max element has %d row and  %d col and value %d\n", maxi+1, maxj+1, array[maxi][maxj]);
    return 0;
}