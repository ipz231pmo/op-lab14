﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 5
#define K 6
int array[K][N];
// Стовпець кандидат 5 n
// Рядок населений пункт 6 k
void randomizeArray() {
    for (int i = 0; i < K; i++)
        for (int j = 0; j < N; j++)
            array[i][j] = rand() % 20 + 50;
}
void printArray() {
    printf("array\n");
    for (int i = 0; i < K; i++) {
        for (int j = 0; j < N; j++)
            printf("%2d  ", array[i][j]);
        printf("\n");
    }
}
int main(){
    srand(time(0));
    randomizeArray();
    printArray();
    int maxj = 0, minj = 0;
    for (int j = 0; j < N; j++) {
        if (array[3][maxj] < array[3][j]) maxj = j;
        if (array[3][minj] > array[3][j]) minj = j;
    }
    printf("Candidate with the least number of votes is %d, with the highest number of votes is %d", minj + 1, maxj + 1);
    return 0;
}