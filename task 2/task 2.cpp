﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 5
#define M 5

void randomizeArray(float array[][5]) {
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++)
            array[i][j] = rand() % 2001 / 100. - 10;
}

void printArray(float array[][5]) {
    printf("array\n");
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++)
            printf("%6.2f ", array[i][j]);
        printf("\n");
    }
    printf("\n");
}

void calcAverage(float array[][M]) {
    for (int i = 0; i < N; i++) {
        float average = 0;
        for (int j = 0; j < M; j++) {
            average += array[i][j];
        }
        average /= M;
        printf("Average of numbers of %d row  is %f\n", i + 1, average);
    }
}

void findMax(float array[][M]) {
    int maxi = 0, maxj = 0;
    for (int i = 0; i < N; i++) 
        for (int j = 0; j < M; j++)
            if (array[i][j] > array[maxi][maxj]) {
                maxi = i;
                maxj = j;
            }
    printf("Max element is %f, it's position is %d row and %d col\n", array[maxi][maxj], maxi + 1, maxj + 1);
}

void findMin(float array[][M]) {
    int mini = 0, minj = 0;
    for (int i = 0; i < N; i++) 
        for (int j = 0; j < M; j++) 
            if (array[i][j] < array[mini][minj]) {
                mini = i;
                minj = j;
            }    
    printf("Min element is %f, it's position is %d row and %d col\n", array[mini][minj], mini + 1, minj + 1);
}

void calcMainDiagonalProdAndSum(float array[][M]) {
    float prod = 1, sum = 0;
    for (int i = 0; i < N; i++) {
        sum += array[i][i];
        prod *= array[i][i];
    }
    printf("Main diagonal sum is %f\n", sum);
    printf("Main diagonal product is %f\n", prod);
}

void calcSumUnderMainDiagonal(float array[][5]) {
    float sum = 0;
    for (int j = 0; j < M; j++)
        for (int i = j + 1; i < N; i++) {
            sum += array[i][j];
#ifdef _DEBUG
            printf("%d  %d\n", i, j);
#endif // !_DEBUG            
        }
    printf("Sum of elements under main diagonal is %f\n", sum);
}

int main() {
    srand(time(0));
    float array[5][5];
    randomizeArray(array);
    printArray(array);    
    findMax(array);
    findMin(array);
    calcAverage(array);
    calcMainDiagonalProdAndSum(array);
    calcSumUnderMainDiagonal(array);
    return 0;
}