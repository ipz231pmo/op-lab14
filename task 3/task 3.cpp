﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 100
#define M 100
int size;
void randomizeArray(int a, int b,int array[N][M]) {
    for (int i = 0; i < size; i++)
        for (int j = 0; j < M; j++)
            array[i][j] = a + rand() % (b-a+1);
}
void printArray(int array[N][M]) {
    printf("array\n");
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++)
            printf("%4d ", array[i][j]);
        printf("\n");
    }
    printf("\n");
}
int main() {
    srand(time(0));
    int a, b, arr[N][M] = {0};
    printf("Enter size: "); scanf("%d", &size);
    if (size > N || size > M || size < 1) {
        printf("Wrong size\n");
        return 0;
    }
    printf("Enter a and b: "); scanf("%d %d", &a, &b);
    if (b < a) {
        printf("B must be bigger than A\n");
        return 0;
    }
    randomizeArray(a, b, arr);
    printArray(arr);
    for (int j = size - 1; j >= 0; j--)
        arr[j][size-1] = arr[0][j] + arr[size-1][j];
    printArray(arr);
    return 0;
}